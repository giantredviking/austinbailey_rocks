# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page_widgets', '0002_servicewidget_thumb'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicewidget',
            name='description',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='servicewidget',
            name='text',
            field=models.TextField(null=True),
        ),
    ]
