# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page_widgets', '0004_servicewidget_title_full'),
    ]

    operations = [
        migrations.RenameField(
            model_name='servicewidget',
            old_name='text',
            new_name='home_text',
        ),
    ]
