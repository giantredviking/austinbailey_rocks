# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page_widgets', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicewidget',
            name='thumb',
            field=models.FileField(null=True, upload_to=b''),
        ),
    ]
