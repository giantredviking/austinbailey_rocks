# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page_widgets', '0005_auto_20150503_2158'),
    ]

    operations = [
        migrations.CreateModel(
            name='HowWidget',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, null=True)),
                ('image', models.FileField(null=True, upload_to=b'')),
                ('description', models.TextField(null=True)),
                ('active', models.BooleanField(default=False)),
            ],
        ),
    ]
