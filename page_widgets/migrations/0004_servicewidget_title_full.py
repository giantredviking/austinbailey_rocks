# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page_widgets', '0003_auto_20150503_2156'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicewidget',
            name='title_full',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
