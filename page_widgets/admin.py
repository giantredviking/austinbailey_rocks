from django.contrib import admin
from django import forms
from ckeditor.widgets import CKEditorWidget

from page_widgets.models import ServiceWidget, HomeDescription, CTAPopUp, HowWidget

class ServiceWidgetForm(forms.ModelForm):
	home_text = forms.CharField(widget=CKEditorWidget())
	description = forms.CharField(widget=CKEditorWidget())

	class Meta:
		model = ServiceWidget
		fields = ['title', 'title_full', 'thumb', 'image', 'home_text', 'description', 'active']

class ServiceWidgetAdmin(admin.ModelAdmin):
    form = ServiceWidgetForm

class HomeDescriptionForm(forms.ModelForm):
	text = forms.CharField(widget=CKEditorWidget())

	class Meta:
		model = HomeDescription
		fields = ['title', 'text', 'active']

class HomeDescriptionAdmin(admin.ModelAdmin):
	form = HomeDescriptionForm

class CTAPopUpForm(forms.ModelForm):
	text = forms.CharField(widget=CKEditorWidget())

	class Meta:
		model = CTAPopUp
		fields = ['title', 'text', 'button_text', 'active']

class CTAPopUpAdmin(admin.ModelAdmin):
	form = CTAPopUpForm

class HowWidgetForm(forms.ModelForm):
	title = forms.CharField(widget=CKEditorWidget())
	description = forms.CharField(widget=CKEditorWidget())

	class Meta:
		model = HowWidget
		fields = ['title', 'image', 'description', 'active']

class HowWidgetAdmin(admin.ModelAdmin):
	form = HowWidgetForm

admin.site.register(ServiceWidget, ServiceWidgetAdmin)
admin.site.register(HomeDescription, HomeDescriptionAdmin)
admin.site.register(CTAPopUp, CTAPopUpAdmin)
admin.site.register(HowWidget, HowWidgetAdmin)