from django.db import models

class ServiceWidget(models.Model):
	title = models.CharField(max_length=100, null=True)
	title_full = models.CharField(max_length=100, null=True)
	thumb = models.FileField(null=True)
	image = models.FileField(null=True)
	home_text = models.TextField(null=True)
	description = models.TextField(null=True)
	active = models.BooleanField(default=False)

	def __unicode__(self):
		return self.title

class HowWidget(models.Model):
	title = models.CharField(max_length=100, null=True)
	image = models.FileField(null=True)
	description = models.TextField(null=True)
	active = models.BooleanField(default=False)

	def __unicode__(self):
		return self.title

class HomeDescription(models.Model):
	title = models.CharField(max_length=100, null=True)
	text = models.TextField()
	active = models.BooleanField(default=False)

	def __unicode__(self):
		return self.title

class CTAPopUp(models.Model):
	title = models.CharField(max_length=255, null=True)
	text = models.TextField()
	button_text = models.CharField(max_length=100, null=True)
	active = models.BooleanField(default=False)

	def __unicode__(self):
		return self.title