from storages.backends.s3boto import S3BotoStorage
from django.conf import settings

StaticRootS3BotoStorage = lambda: S3BotoStorage(
	bucket=settings.AWS_STORAGE_BUCKET_NAME
)

MediaStorage = lambda: S3BotoStorage(
	bucket=settings.AWS_MEDIA_BUCKET_NAME
)