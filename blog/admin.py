from django.contrib import admin
from django import forms
from ckeditor.widgets import CKEditorWidget

from blog.models import Article

class ArticleForm(forms.ModelForm):
	content = forms.CharField(widget=CKEditorWidget())

	class Meta:
		model = Article
		fields = ['title', 'slug', 'author', 'image', 'content']

class ArticleAdmin(admin.ModelAdmin):
	form = ArticleForm

admin.site.register(Article, ArticleAdmin)