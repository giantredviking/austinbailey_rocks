from django.db import models

# Create your models here.
class Article(models.Model):
	title = models.CharField(max_length=255, null=True)
	slug = models.CharField(max_length=255, null=True)
	author = models.CharField(max_length=100, null=True)
	image = models.FileField(null=True)
	published = models.DateField(auto_now_add=True)
	content = models.TextField()

	def __unicode__(self):
		return self.title