# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, null=True)),
                ('author', models.CharField(max_length=100, null=True)),
                ('image', models.FileField(null=True, upload_to=b'')),
                ('published', models.DateField(auto_now_add=True)),
                ('content', models.TextField()),
            ],
        ),
    ]
