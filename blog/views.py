from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse

from blog.models import Article

# Create your views here.
def indexView(request):
	context = {}
	articles = Article.objects.all().order_by('published')

	context['articles'] = articles
	return render(request, 'blog.html', context)

def articleView(request, slug):
	context = {}

	try:
		article = Article.objects.get(slug=slug)
		context['article'] = article
	except Article.DoesNotExist:
		return redirect(reverse('blog:index'))

	return render(request, 'post.html', context)