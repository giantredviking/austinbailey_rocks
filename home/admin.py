from django.contrib import admin
from django import forms
from ckeditor.widgets import CKEditorWidget

from home.models import FrontImage, ContactLead

class FrontImageForm(forms.ModelForm):
	text = forms.CharField(widget=CKEditorWidget())

	class Meta:
		model = FrontImage
		fields = ['image', 'text', 'active']

class FrontImageAdmin(admin.ModelAdmin):
    form = FrontImageForm

class ContactLeadForm(forms.ModelForm):
	class Meta:
		model = ContactLead
		fields = ['name', 'first_name', 'last_name', 'company', 'email', 'phone', 'need']

class ContactLeadAdmin(admin.ModelAdmin):
	form = ContactLeadForm

admin.site.register(FrontImage, FrontImageAdmin)
admin.site.register(ContactLead, ContactLeadAdmin)