from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static

import views

urlpatterns = [
    url(r'^$', views.indexView, name='index'),
    url(r'^what/$', views.whatView, name='what'),
    url(r'^who/$', views.whoView, name='who'),
    url(r'^contact/$', views.contactView, name='contact'),
    url(r'^lead/$', views.lead_handler, name='lead'),
]

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)