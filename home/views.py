from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib import messages
import json

from home.forms import ContactForm, CTAForm
from page_widgets.models import ServiceWidget, HomeDescription, CTAPopUp, HowWidget
from home.models import ContactLead
from blog.models import Article
from home.utils import send_email

# Create your views here.
def indexView(request):
	context = {}
	form = ContactForm()

	try:
		services = ServiceWidget.objects.filter(active=True)[:3]
	except ServiceWidget.DoesNotExist:
		services = False

	try:
		home_desc = HomeDescription.objects.get(active=True)
	except HomeDescription.DoesNotExist:
		home_desc = False
		
	try:
		cta = CTAPopUp.objects.get(active=True)
	except CTAPopUp.DoesNotExist:
		cta = False

	cta_form = CTAForm()

	if request.session.get('lead'):
		context['lead'] = True

	context['form'] = form
	context['cta'] = cta
	context['cta_form'] = cta_form
	context['services'] = services
	context['home_desc'] = home_desc
	return render(request, 'index.html', context)

def whatView(request):
	context = {}
	form = ContactForm()

	try:
		services = ServiceWidget.objects.filter(active=True)[:3]
	except ServiceWidget.DoesNotExist:
		services = False

	try:
		how = HowWidget.objects.get(active=True)
	except HowWidget.DoesNotExist:
		how = False

	context['form'] = form
	context['how'] = how
	context['services'] = services
	return render(request, 'what.html', context)

def whoView(request):
	context = {}
	form = ContactForm()
	cta_form = CTAForm()

	try:
		cta = CTAPopUp.objects.get(active=True)
	except CTAPopUp.DoesNotExist:
		cta = False

	if request.session.get('lead'):
		context['lead'] = True

	context['cta'] = cta
	context['form'] = form
	context['cta_form'] = cta_form
	return render(request, 'who.html', context)

def contactView(request):
	context = {}
	form = ContactForm()

	context['form'] = form
	return render(request, 'contact.html', context)

def lead_handler(request):
	if request.method == 'POST':
		if 'cta_submit' in request.POST:
			request.session['lead'] = True

			name = request.POST.get('name')
			email = request.POST.get('email')
			phone = request.POST.get('phone')

			lead = ContactLead.objects.create(name=name, first_name="cta", last_name="cta", company="cta", email=email, phone=phone, need="cta request")
			lead.save()

			send_email()
			messages.add_message(request, messages.SUCCESS, "Thank you for sending me your information! I'll be in touch with you within the next 24 hours.")
			return redirect(reverse('home:index'))
		if 'contact_submit' in request.POST:
			first_name = request.POST.get('first_name')
			last_name = request.POST.get('last_name')
			company = request.POST.get('company')
			email_addr = request.POST.get('email')
			phone_num = request.POST.get('phone')
			need = request.POST.get('need')
			name = first_name + " " + last_name

			lead = ContactLead.objects.create(name=name, first_name=first_name, last_name=last_name, company=company, email=email_addr, phone=phone_num, need=need)
			lead.save()

			send_email()
			messages.add_message(request, messages.SUCCESS, "Thank you for sending me your information! I'll be in touch with you within the next 24 hours.")
			return redirect(reverse('home:index'))
	else:
		return redirect(reverse('home:index'))