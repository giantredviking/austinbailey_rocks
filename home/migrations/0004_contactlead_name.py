# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_contactlead'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactlead',
            name='name',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
