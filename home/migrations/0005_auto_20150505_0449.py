# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0004_contactlead_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactlead',
            name='phone',
            field=models.CharField(max_length=15, null=True),
        ),
    ]
