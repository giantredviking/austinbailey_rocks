# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_auto_20150505_0449'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactlead',
            name='company',
            field=models.CharField(default='migrate', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contactlead',
            name='first_name',
            field=models.CharField(default='migrate', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contactlead',
            name='last_name',
            field=models.CharField(default='migrate', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contactlead',
            name='name',
            field=models.CharField(default='migrate', max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contactlead',
            name='need',
            field=models.TextField(default='migrate'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contactlead',
            name='phone',
            field=models.CharField(default='migrate', max_length=15),
            preserve_default=False,
        ),
    ]
