from django import forms
from django.utils.translation import ugettext_lazy as _

from home.models import ContactLead

class ContactForm(forms.ModelForm):
	class Meta:
		model = ContactLead
		fields = ['first_name', 'last_name', 'company', 'email', 'phone', 'need']
		labels = {
			'first_name': _('First Name'),
			'last_name': _('Last Name'),
			'company': _('Company'),
			'email': _('Email Address'),
			'phone': _('Phone Number'),
			'need': _('How Can I Help?'),
		}
		widgets = {
			'first_name': forms.TextInput(attrs={'placeholder': 'Austin'}),
			'last_name': forms.TextInput(attrs={'placeholder': 'Bailey'}),
			'company': forms.TextInput(attrs={'placeholder': 'Company XYZ'}),
			'email': forms.EmailInput(attrs={'placeholder': 'austin@austinbailey.rocks'}),
			'phone': forms.TextInput(attrs={'placeholder': '(704) 351-1478'}),
			'need': forms.Textarea(attrs={'placeholder': 'Website redesign, online marketing, social media, etc.'}),
		}

class CTAForm(forms.Form):
	name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Austin Bailey'}))
	email = forms.CharField(widget=forms.EmailInput(attrs={'placeholder': 'austin@austinbailey.rocks'}))
	phone = forms.CharField(widget=forms.TextInput(attrs={'placeholder': '(704) 351-1478'}))