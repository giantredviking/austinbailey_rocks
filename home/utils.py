from django.core.mail import send_mail

from home.models import ContactLead

def send_email():
	lead = ContactLead.objects.latest('id')
	send_mail('New Contact Lead', 'Name: ' + lead.name + '\nPhone: ' + lead.phone + 
		'\nEmail: ' + lead.email + '\nComapany: ' + lead.company + '\nNeed: ' + lead.need,
		'baileyaustinw@gmail.com', ['austin@austinbailey.rocks'], fail_silently=False)