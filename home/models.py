from django.db import models
from django.core.mail import send_mail
from django.dispatch import receiver
from django.db.models.signals import post_save

class FrontImage(models.Model):
	image = models.FileField()
	active = models.BooleanField()
	text = models.TextField(max_length=140, null=True)

	def __unicode__(self):
		return self.text

class ContactLead(models.Model):
	name = models.CharField(max_length=255)
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	company = models.CharField(max_length=50)
	email = models.EmailField(null=True)
	phone = models.CharField(max_length=15)
	need = models.TextField()

	def __unicode__(self):
		return self.name